#!/bin/bash
rm -f test_mf.lib?.*
rm -f test_mf.out.bed
rm -f test_mf.out.var
rm -f test_mf.hap*.fa 
rm -f egTree.tree.lib?.*
rm -f egTree.tree.out.*
rm -f egTree.tree.plot
rm -f egTree.tree.var
rm -f egTree.tree.json
rm -f egTree.tree.hap*.fa
